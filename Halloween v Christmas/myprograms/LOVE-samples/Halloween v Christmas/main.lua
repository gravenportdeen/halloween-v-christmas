function love.load()
  castleBackground = love.graphics.newImage("sprites/castlebackground.png")
  castleQuad = love.graphics.newQuad(0, 0, 480, 270, 480, 270)
  forestBackground = love.graphics.newImage("sprites/forestbackground.png")
  forestQuad = love.graphics.newQuad(0, 0, 480, 270, 480, 270)
  factoryBackground = love.graphics.newImage("sprites/factorybackground.png")
  factoryQuad = love.graphics.newQuad(0, 0, 480, 270, 480, 270)
  title = love.graphics.newImage("sprites/titlescreen.png")
  titleQuad = love.graphics.newQuad(0, 0, 480, 270, 480, 270)
  instruction = love.graphics.newImage("sprites/instructions.png")
  instructionQuad = love.graphics.newQuad(0, 0, 480, 270, 480, 270)
  
  --variables for game. dan
  groundHeight = 270
  player = 1
  gameState = 0 -- 0 for menu, 1 for instructions, 2 for level, 3 for level clear, 4 for game over, 5 for game clear
  level = 0
  screenMoved = 0
  totalEnemies = 0
  playersSwapped = false
  playerCanMove = false
  playerMoveDirection = -1
  breakFunction = false
  
  players = {}
  
  --variables about the skeleton. dan
  skeletonSpritesheet = love.graphics.newImage("sprites/skeleton.png")
  
  --variables about the witch. dan
  witchSpritesheet = love.graphics.newImage("sprites/witch.png")
  
  --variables about the snowman. dan
  snowmanSpritesheet = love.graphics.newImage("sprites/snowman.png")
  
  -- Snowballs. joe
  snowball = love.graphics.newImage("sprites/snowball.png")
  snowballs = {} -- holds snowballs
  
  fireball = love.graphics.newImage("sprites/fireball.png")
  fireballs = {}
  
  objects = {}
  
  -- Crates. joe
  present = love.graphics.newImage("sprites/crate.png")
  
  -- Conveyor belts. joe
  beltSpritesheet = love.graphics.newImage("sprites/belt.png")
  
  platformSpritesheet = love.graphics.newImage("sprites/platform.png")
  
  buttonSpritesheet = love.graphics.newImage("sprites/buttons.png")
  buttonQuad = love.graphics.newQuad(0, 0, 40, 20, 80, 20)
  buttonAttackQuad = love.graphics.newQuad(0, 0, 40, 20, 80, 20)
  buttonJumpQuad = love.graphics.newQuad(0, 0, 40, 20, 80, 20)
  buttonAttackFrames = 0
  buttonJumpFrames = 0
  
  fireballSound = love.audio.newSource("sounds/fireball.ogg", "static")
  snowballHitSound = love.audio.newSource("sounds/snowballhit.ogg", "static")
  snowCannonSound = love.audio.newSource("sounds/snowcannon.ogg", "static")
  swordSwingSound = love.audio.newSource("sounds/swordswing.ogg", "static")
end

function love.mousepressed(x, y, button, isTouch) -- dan
  breakFunction = false
  
  if x <= 40 then --if button is pressed change states between moving and attacking
    if y >= 250 then
      --if isTouch then
        for i,v in ipairs(players) do
          if player == v.playerType then
            if v.state == 0 then
              if v.playerType ~= 2 then
                v.state = 3
                v.frames = 30
                if v.playerType == 1 then
                  swordSwingSound:play()
                end
              else
                v.attacking = true
              end
            elseif v.state == 3 then
              if v.playerType ~= 1 then
                v.frames = 30
                if v.playerType == 5 then -- use for snowman attack
                  v.sprite = 2
                  v.rot = 0
                  v.toStanding = true
                else
                  v.state = 0
                end
              end
            end
          end
        end
        buttonAttackFrames = 10
        breakFunction = true
      --end
    end
  end
  
  if x >= 440 then
    if y >= 250 then
      --if isTouch then
        for i,v in ipairs(players) do
          if player == v.playerType then
            if v.posY == v.ground - v.height then
              if v.state == 0 then
                v.state = 1
                v.jumpHeight = 15
              end
            end
          end
        end
        buttonJumpFrames = 10
        breakFunction = true
      --end
    end
  end
  
  if breakFunction == false then
    for i,v in ipairs(players) do
      if player == v.playerType then
        if v.dir == -1 then
          if player == 1 then
            posX = v.posX + 9
            width = v.width - 9
          else
            posX = v.posX
            width = v.width
          end
        else
          if player == 1 then
            posX = v.posX - v.width
            width = v.width - 9
          else
            posX = v.posX - v.width
            width = v.width
          end
        end
        
        if x >= posX then
          if x <= posX + width then
            if y >= v.posY then
              if y <= v.posY + v.height then
                --if isTouch then
                  if playersSwapped == false then
                    if gameState == 2 then
                      for i,v in ipairs(players) do
                        if player == v.playerType then
                          posX = v.posX
                          posY = v.posY
                          dir = v.dir
                        end
                      end
                      player = player + 1
                      if player == 3 then
                        player = 1
                      else
                        playersSwapped = true
                      end
                      for i,v in ipairs(players) do
                        if player == 1 then
                          if player == v.playerType then
                            if dir == -1 then
                              v.posX = posX
                              v.posY = posY
                              v.dir = dir
                            else
                              v.posX = posX + 9
                              v.posY = posY
                              v.dir = dir
                            end
                          end
                        else
                          if player == v.playerType then
                            if dir == -1 then
                              v.posX = posX
                              v.posY = posY
                              v.dir = dir
                            else
                              v.posX = posX - 9
                              v.posY = posY
                              v.dir = dir
                            end
                          end
                        end
                      end
                      breakFunction = true
                    end
                  end
                --end
              end
            end
          end
        end
      end
    end
    playersSwapped = false
  end
  
  if breakFunction == false then
    --if isTouch then
      for i,v in ipairs(players) do
        if player == v.playerType then
          if player == 1 then
            if v.dir == -1 then
              posX = v.posX
            else
              posX = v.posX - v.width - 9
            end
          else
            if v.dir == -1 then
              posX = v.posX
            else
              posX = v.posX - v.width
            end
          end
          if x < posX then
            playerCanMove = true
            playerMoveDirection = -1
          elseif x > posX + v.width then
            playerCanMove = true
            playerMoveDirection = 1
          end
        end
      end
    --end
  end
  
  if x >= 250 then
    if x <= 330 then
      if y >= 170 then
        if y <= 210 then
          --if isTouch then
            if gameState == 0 then
              gameState = 1
            elseif gameState == 1 then
              CreateCastleLevel()
            elseif gameState == 3 then
              level = level + 1
              for i,v in ipairs(players) do
                players[i] = nil
              end
              for i,v in ipairs(objects) do
                objects[i] = nil
              end
              for i,v in ipairs(snowballs) do
                snowballs[i] = nil
              end
              if level == 1 then
                CreateForestLevel()
              elseif level == 2 then
                CreateFactoryLevel()
              elseif level == 3 then
                gameState = 5
              end
            elseif gameState == 4 then
              gameState = 0
              for i,v in ipairs(players) do
                players[i] = nil
              end
              for i,v in ipairs(objects) do
                objects[i] = nil
              end
              for i,v in ipairs(snowballs) do
                snowballs[i] = nil
              end
            elseif gameState == 5 then
              gameState = 0
              for i,v in ipairs(players) do
                players[i] = nil
              end
              for i,v in ipairs(objects) do
                objects[i] = nil
              end
              for i,v in ipairs(snowballs) do
                snowballs[i] = nil
              end
            end
          --end
        end
      end
    end
  end
end

function love.mousereleased(x, y, button, isTouch)
  playerCanMove = false
end

function love.update(dt)
  if gameState == 2 then
    CheckGround()
    UpdatePlayer()
    UpdateObjects()
    CheckCeilings()
    CheckLeftWalls()
    CheckRightWalls()
  end
  
  for i,v in ipairs(players) do
    if player == v.playerType then
      if v.health <= 0 then
        gameState = 4
        table.remove(players, i)
      end
    else
      if v.health <= 0 then
        totalEnemies = totalEnemies - 1
        table.remove(players, i)
      end
    end
  end
  
  if gameState == 2 then
    if totalEnemies == 0 then
      if level == 2 then
        gameState = 5
      else
        gameState = 3
      end
    end
  end
  
  if buttonAttackFrames > 0 then
    buttonAttackFrames = buttonAttackFrames - 1
  end
  
  if buttonJumpFrames > 0 then
    buttonJumpFrames = buttonJumpFrames - 1
  end
end

function love.draw()  
  if level == 0 then
    love.graphics.draw(castleBackground, castleQuad, 0, 0)
  elseif level == 1 then
    love.graphics.draw(forestBackground, forestQuad, 0, 0)
  elseif level == 2 then
    love.graphics.draw(factoryBackground, factoryQuad, 0, 0)
  end
  
  for i,v in ipairs(objects) do
    if v.objectType == 0 then
      love.graphics.draw(present, v.x, v.y)
    elseif v.objectType == 1 then
      love.graphics.draw(beltSpritesheet, v.quad, v.x, v.y)
    elseif v.objectType == 2 then
      love.graphics.draw(platformSpritesheet, v.quad, v.x, v.y)
    end
  end
  
  -- draw player and offset origin for rotation
  for i,v in ipairs(players) do
    if v.playerType == 1 then
      if player == 1 then
        love.graphics.draw(skeletonSpritesheet, v.quad, v.posX, v.posY, 0, -v.dir, 1)
      end
    elseif v.playerType == 2 then
      if player == 2 then
        love.graphics.draw(witchSpritesheet, v.quad, v.posX, v.posY, 0, -v.dir, 1)
      end
    elseif v.playerType == 5 then
      love.graphics.draw(snowmanSpritesheet, v.quad, v.posX + 18.5, v.posY + 28, v.rot, -v.dir, 1, 18.5, 28)
    end
  end
  
  for i,v in ipairs(snowballs) do -- draw snowballs. joe
    love.graphics.draw(snowball, v.x, v.y)
  end
  
  for i,v in ipairs(fireballs) do
    love.graphics.draw(fireball, v.x, v.y)
  end
  
  if gameState == 0 then
    love.graphics.draw(title, titleQuad, 0, 0)
  elseif gameState == 1 then
    love.graphics.draw(instruction, instructionQuad, 0, 0)
  elseif gameState == 2 then
    for i,v in ipairs(players) do
      if player == v.playerType then
        health = v.health
      end
    end
    love.graphics.setColor(0, 255, 0, 255)
    love.graphics.rectangle("fill", 0, 0, health * 2, 20)
    love.graphics.setColor(0, 0, 0, 255)
    love.graphics.print("HEALTH", 0, 0)
    love.graphics.setColor(255, 255, 255, 250)
    
    if buttonAttackFrames > 0 then
      buttonAttackQuad = love.graphics.newQuad(40, 0, 40, 20, 80, 20)
    else
      buttonAttackQuad = love.graphics.newQuad(0, 0, 40, 20, 80, 20)
    end
    
    if buttonJumpFrames > 0 then
      buttonJumpQuad = love.graphics.newQuad(40, 0, 40, 20, 80, 20)
    else
      buttonJumpQuad = love.graphics.newQuad(0, 0, 40, 20, 80, 20)
    end
    
    love.graphics.draw(buttonSpritesheet, buttonAttackQuad, 0, 250)
    love.graphics.draw(buttonSpritesheet, buttonJumpQuad, 440, 250)
  elseif gameState == 3 then
    love.graphics.print("LEVEL CLEAR", 100, 100)
  elseif gameState == 4 then
    love.graphics.print("GAME OVER", 100, 100)
  elseif gameState == 5 then
    love.graphics.print("GAME CLEAR. CONGRATULATIONS", 100, 100)
  end
  
  if gameState ~= 2 then
    love.graphics.draw(buttonSpritesheet, buttonQuad, 250, 170, 0, 2)
    love.graphics.print("CONTINUE", 260, 182)
  end
end

function CreateCastleLevel()
  gameState = 2
  level = 0
  screenMoved = 0
  
  skeleton = {}
  skeleton.quad = love.graphics.newQuad(0, 0, 28, 38, 336, 38)
  skeleton.sprite = 0
  skeleton.width = 28
  skeleton.height = 38
  skeleton.speed = 3
  skeleton.previousGround = 0
  skeleton.ground = groundHeight
  skeleton.posX = 400 -- when dir = 1, posX is moved forward 9
  skeleton.posY = skeleton.ground - skeleton.height
  skeleton.dir = -1 --player faces left, variable is 1 if the player face right
  skeleton.state = 0
  skeleton.executeAttack = false
  skeleton.frames = 30 --frame counter
  skeleton.jumpHeight = 0
  skeleton.health = 100
  skeleton.playerType = 1 -- 1 for skeleton
  table.insert(players, skeleton)
  
  witch = {}
  witch.quad = love.graphics.newQuad(0, 0, 28, 38, 308, 38)
  witch.sprite = 0
  witch.width = 28
  witch.height = 38
  witch.speed = 2
  witch.previousGround = 0
  witch.ground = groundHeight
  witch.posX = 400
  witch.posY = witch.ground - witch.height
  witch.dir = -1 --player faces left, variable is 1 if the player face right
  witch.state = 0
  witch.executeAttack = false
  witch.frames = 30 --frame counter
  witch.standing = true
  witch.attacking = false
  witch.jumpHeight = 0
  witch.health = 100
  witch.playerType = 2 -- 2 for witch
  table.insert(players, witch)
  
  for i=0,2 do
    snowman = {}
    snowman.quad = love.graphics.newQuad(0, 0, 26, 38, 104, 38)
    snowman.sprite = 0
    snowman.toStanding = false --variable is true when snowman is transitioning from attack to moving
    snowman.width = 26
    snowman.height = 38
    snowman.speed = 3
    snowman.previousGround = 0
    snowman.ground = groundHeight
    snowman.posX = i * 100 -- when dir = 1, posX is moved back 11
    snowman.posY = snowman.ground - snowman.height
    snowman.rot = 0
    snowman.executeRotate = false
    snowman.rotating = false
    snowman.executeAttack = false
    snowman.dir = 1 --player faces left, variable is 1 if the player face right
    snowman.state = 3
    snowman.identity = i
    snowman.frames = 30 --frame counter
    snowman.jumpHeight = 0
    snowman.invincibilityFrames = 0
    snowman.health = 100
    snowman.playerType = 5 -- 5 for snowman
    totalEnemies = totalEnemies + 1
    table.insert(players, snowman)
  end
  
  for i=0,5 do
    crate = {}
    crate.width = 40
    crate.height = 40
    crate.x = i * 200 + 50
    crate.y = groundHeight - crate.height
    crate.groundFound = false
    crate.checkLeftWall = true
    crate.checkRightWall = true
    crate.objectType = 0 -- 0 for crate
    table.insert(objects, crate)
  end
  
  buttonAttackFrames = 0
  buttonJumpFrames = 0
end

function CreateForestLevel()
  gameState = 2
  level = 1
  screenMoved = 0
  
  skeleton = {}
  skeleton.quad = love.graphics.newQuad(0, 0, 28, 38, 336, 38)
  skeleton.sprite = 0
  skeleton.width = 28
  skeleton.height = 38
  skeleton.speed = 3
  skeleton.previousGround = 0
  skeleton.ground = groundHeight
  skeleton.posX = 400
  skeleton.posY = skeleton.ground - skeleton.height
  skeleton.dir = -1 --player faces left, variable is 1 if the player face right
  skeleton.state = 0
  skeleton.executeAttack = false
  skeleton.frames = 30 --frame counter
  skeleton.jumpHeight = 0
  skeleton.health = 100
  skeleton.playerType = 1 -- 1 for skeleton
  table.insert(players, skeleton)
  
  witch = {}
  witch.quad = love.graphics.newQuad(0, 0, 28, 38, 308, 38)
  witch.sprite = 0
  witch.width = 28
  witch.height = 38
  witch.speed = 2
  witch.previousGround = 0
  witch.ground = groundHeight
  witch.posX = 400
  witch.posY = witch.ground - witch.height
  witch.dir = -1 --player faces left, variable is 1 if the player face right
  witch.state = 0
  witch.executeAttack = false
  witch.frames = 30 --frame counter
  witch.standing = true
  witch.attacking = false
  witch.jumpHeight = 0
  witch.health = 100
  witch.playerType = 2 -- 2 for witch
  table.insert(players, witch)
  
  for i=0,2 do
    snowman = {}
    snowman.quad = love.graphics.newQuad(0, 0, 26, 38, 104, 38)
    snowman.sprite = 0
    snowman.toStanding = false --variable is true when snowman is transitioning from attack to moving
    snowman.width = 26
    snowman.height = 38
    snowman.speed = 3
    snowman.previousGround = 0
    snowman.ground = groundHeight
    snowman.posX = i * 100
    snowman.posY = snowman.ground - snowman.height
    snowman.rot = 0
    snowman.executeRotate = false
    snowman.rotating = false
    snowman.executeAttack = false
    snowman.dir = 1 --player faces left, variable is 1 if the player face right
    snowman.state = 3
    snowman.identity = i
    snowman.frames = 30 --frame counter
    snowman.jumpHeight = 0
    snowman.invincibilityFrames = 0
    snowman.health = 100
    snowman.playerType = 5 -- 5 for snowman
    totalEnemies = totalEnemies + 1
    table.insert(players, snowman)
  end
  
  for i=0,11 do
    platform = {}
    platform.width = 38
    platform.height = 8
    
    if i == 0 then
      platform.quad = love.graphics.newQuad(0, 0, 38, 8, 114, 8)
      platform.x = 0
    elseif i == 4 then 
      platform.quad = love.graphics.newQuad(0, 0, 38, 8, 114, 8) 
      platform.x = 300
    elseif i == 8 then  
      platform.quad = love.graphics.newQuad(0, 0, 38, 8, 114, 8)  
      platform.x = 600
    elseif i == 3 then   
      platform.quad = love.graphics.newQuad(2 * platform.width, 0, 38, 8, 114, 8) 
      platform.x = 0 + platform.width * 3
    elseif i == 7 then 
      platform.quad = love.graphics.newQuad(2 * platform.width, 0, 38, 8, 114, 8) 
      platform.x = 300 + platform.width * 3
    elseif i == 11 then 
      platform.quad = love.graphics.newQuad(2 * platform.width, 0, 38, 8, 114, 8)
      platform.x = 600 + platform.width * 3
    elseif i == 1 then
      platform.quad = love.graphics.newQuad(platform.width, 0, 38, 8, 114, 8)
      platform.x = platform.width
    elseif i == 2 then
      platform.quad = love.graphics.newQuad(platform.width, 0, 38, 8, 114, 8)
      platform.x = platform.width * 2
    elseif i == 5 then
      platform.quad = love.graphics.newQuad(platform.width, 0, 38, 8, 114, 8)
      platform.x = platform.width + 300
    elseif i == 6 then
      platform.quad = love.graphics.newQuad(platform.width, 0, 38, 8, 114, 8)
      platform.x = platform.width * 2 + 300
    elseif i == 9 then
      platform.quad = love.graphics.newQuad(platform.width, 0, 38, 8, 114, 8)
      platform.x = platform.width + 600
    elseif i == 10 then
      platform.quad = love.graphics.newQuad(platform.width, 0, 38, 8, 114, 8)
      platform.x = platform.width * 2 + 600
    else
      platform.quad = love.graphics.newQuad(platform.width, 0, 38, 8, 114, 8)
      platform.x = 100
    end
    
    platform.y = 170
    platform.groundFound = false
    platform.checkLeftWall = true
    platform.checkRightWall = true
    platform.sprite = 0
    platform.objectType = 2 -- 2 for platform
    table.insert(objects, platform)
  end
  
  buttonAttackFrames = 0
  buttonJumpFrames = 0
end

function CreateFactoryLevel()
  gameState = 2
  level = 2
  screenMoved = 0
  
  skeleton = {}
  skeleton.quad = love.graphics.newQuad(0, 0, 28, 38, 336, 38)
  skeleton.sprite = 0
  skeleton.width = 28
  skeleton.height = 38
  skeleton.speed = 3
  skeleton.previousGround = 0
  skeleton.ground = groundHeight
  skeleton.posX = 400
  skeleton.posY = skeleton.ground - skeleton.height
  skeleton.dir = -1 --player faces left, variable is 1 if the player face right
  skeleton.state = 0
  skeleton.executeAttack = false
  skeleton.frames = 30 --frame counter
  skeleton.jumpHeight = 0
  skeleton.health = 100
  skeleton.playerType = 1 -- 1 for skeleton
  table.insert(players, skeleton)
  
  witch = {}
  witch.quad = love.graphics.newQuad(0, 0, 28, 38, 308, 38)
  witch.sprite = 0
  witch.width = 28
  witch.height = 38
  witch.speed = 2
  witch.previousGround = 0
  witch.ground = groundHeight
  witch.posX = 400
  witch.posY = witch.ground - witch.height
  witch.dir = -1 --player faces left, variable is 1 if the player face right
  witch.state = 0
  witch.executeAttack = false
  witch.frames = 30 --frame counter
  witch.standing = true
  witch.attacking = false
  witch.jumpHeight = 0
  witch.health = 100
  witch.playerType = 2 -- 2 for witch
  table.insert(players, witch)
  
  for i=0,2 do
    snowman = {}
    snowman.quad = love.graphics.newQuad(0, 0, 26, 38, 104, 38)
    snowman.sprite = 0
    snowman.toStanding = false --variable is true when snowman is transitioning from attack to moving
    snowman.width = 26
    snowman.height = 38
    snowman.speed = 3
    snowman.previousGround = 0
    snowman.ground = groundHeight
    snowman.posX = i * 100
    snowman.posY = snowman.ground - snowman.height
    snowman.rot = 0
    snowman.executeRotate = false
    snowman.rotating = false
    snowman.executeAttack = false
    snowman.dir = 1 --player faces left, variable is 1 if the player face right
    snowman.state = 3
    snowman.identity = i
    snowman.frames = 30 --frame counter
    snowman.jumpHeight = 0
    snowman.invincibilityFrames = 0
    snowman.health = 100
    snowman.playerType = 5 -- 5 for snowman
    totalEnemies = totalEnemies + 1
    table.insert(players, snowman)
  end
  
  for i=0,2 do
    belt = {}
    belt.quad = love.graphics.newQuad(0, 0, 46, 10, 184, 10)
    belt.width = 46
    belt.height = 10
    belt.x = i * belt.width + 300
    belt.y = 68
    belt.groundFound = false
    if i == 2 then
      belt.checkRightWall = true
    else
      belt.checkRightWall = false
    end
    if i == 0 then
      belt.checkLeftWall = true
    else
      belt.checkLeftWall = false
    end
    belt.sprite = 0 -- 1,2,3 for each belt sprite
    belt.timer = 0
    belt.objectType = 1 -- 1 for belt
    table.insert(objects, belt)
  end
  
  buttonAttackFrames = 0
  buttonJumpFrames = 0
end

function CheckGround()
  for i,v in ipairs(players) do
    dir = v.dir
    x = v.posX
    y = v.posY
    width = v.width
    height = v.height
    playerType = v.playerType
    
    for i,v in ipairs(objects) do
      if dir == -1 then
        if playerType == 1 then
          if x >= v.x - 22 then
            if x <= v.x + v.width - 16 then
              v.groundFound = true
            end
          end
        elseif playerType == 2 then
          if x >= v.x - 19 then
            if x <= v.x + v.width - 13 then
              v.groundFound = true
            end
          end
        elseif playerType == 5 then
          if x >= v.x - 17 then
            if x <= v.x + v.width - 12 then
              v.groundFound = true
            end
          end
        end
      else
        if playerType == 1 then
          if x - 36 >= v.x - 22 then
            if x - 36 <= v.x + v.width - 16 then
              v.groundFound = true
            end
          end
        elseif playerType == 2 then
          if x - 27 >= v.x - 19 then
            if x - 27 <= v.x + v.width - 13 then
              v.groundFound = true
            end
          end
        elseif playerType == 5 then
          if x + 11 >= v.x - 17 then
            if x + 11 <= v.x + v.width - 12 then
              v.groundFound = true
            end
          end
        end
      end
      
      if v.y < y + height then
        v.groundFound = false
      end
    end
    
    v.previousGround = v.ground
    ground = groundHeight
    for i,v in ipairs(objects) do
      if v.groundFound then
        if v.y < ground then
          ground = v.y
        end
      end
      v.groundFound = false
    end
    v.ground = ground
    
    state = v.state
    if y + height < ground then
      if state ~= 1 then
        state = 2
      end
    end
    v.state = state
  end
end

function UpdatePlayer() -- dan
  for i,v in ipairs(players) do
    v.frames = v.frames - 1 -- decrement player frame counter
  end
  
  PlayerSprite()
  PlayerBeltCheck()
  PlayerMove()
  PlayerJump()
  PlayerFall()
  PlayerAttack()
  
  for i,v in ipairs(players) do
    if v.frames == 0 then
      v.frames = 30
    end
    if v.playerType == 5 then
      if v.invincibilityFrames ~= 0 then
        v.invincibilityFrames = v.invincibilityFrames - 1
      end
    end
  end
end

function UpdateObjects()
  UpdateBelts()
  UpdateSnowballs()
  UpdateFireballs()
end

function CheckCeilings()
  for i,v in ipairs(players) do
    dir = v.dir
    x = v.posX
    y = v.posY
    width = v.width
    height = v.height
    jumpHeight = v.jumpHeight
    state = v.state
    playerType = v.playerType
    
    for i,v in ipairs(objects) do
      if dir == -1 then
        if playerType == 1 then
          hitTest = CheckCollision(v.x + 1, v.y + v.height - 1, v.width - 2, 1, x + 9, y, width - 9, height)
        else
          hitTest = CheckCollision(v.x + 1, v.y + v.height - 1, v.width - 2, 1, x, y, width, height)
        end
      else
        if playerType == 1 then
          hitTest = CheckCollision(v.x + 1, v.y + v.height - 1, v.width - 2, 1, x - 27, y, width - 9, height)
        elseif playerType == 2 then
          hitTest = CheckCollision(v.x + 1, v.y + v.height - 1, v.width - 2, 1, x - 27, y, width, height)
        elseif playerType == 5 then
          hitTest = CheckCollision(v.x + 1, v.y + v.height - 1, v.width - 2, 1, x + 11, y, width, height)
        end
      end
      if hitTest then
        if state == 1 then
          y = v.y + v.height
          jumpHeight = 0
          state = 2
        end
      end
    end
    
    v.posY = y
    v.jumpHeight = jumpHeight
    v.state = state
  end
end

function CheckLeftWalls()
  for i,v in ipairs(players) do
    dir = v.dir
    x = v.posX
    y = v.posY
    width = v.width
    height = v.height
    playerType = v.playerType
    
    for i,v in ipairs(objects) do
      if v.checkLeftWall then
        if dir == -1 then
          if playerType == 1 then
            hitTest = CheckCollision(v.x, v.y + 1, 1, v.height - 2, x + 9, y, width - 9, height)
          else
            hitTest = CheckCollision(v.x, v.y + 1, 1, v.height - 2, x, y, width, height)
          end
          if hitTest then
            x = v.x - width
          end
        else
          if playerType == 1 then
            hitTest = CheckCollision(v.x, v.y + 1, 1, v.height - 2, x - 24, y, 15, height)
          elseif playerType == 2 then
            hitTest = CheckCollision(v.x, v.y + 1, 1, v.height - 2, x - 12, y, 12, height)
          elseif playerType == 5 then
            hitTest = CheckCollision(v.x, v.y + 1, 1, v.height - 2, x + width, y, 11, height)
          end
          if hitTest then
            if playerType == 1 then
              x = v.x + 9
            elseif playerType == 2 then
              x = v.x
            elseif playerType == 5 then
              x = v.x - width - 11
            end
          end
        end
      end
    end
    v.posX = x
  end
end

function CheckRightWalls()
  for i,v in ipairs(players) do
    dir = v.dir
    x = v.posX
    y = v.posY
    width = v.width
    height = v.height
    playerType = v.playerType
    
    for i,v in ipairs(objects) do
      if v.checkRightWall then
        if dir == -1 then
          if playerType == 1 then
            hitTest = CheckCollision(v.x + v.width - 1, v.y + 1, 1, v.height - 2, x + 9, y, 15, height)
          elseif playerType == 2 then
            hitTest = CheckCollision(v.x + v.width - 1, v.y + 1, 1, v.height - 2, x, y, 12, height)
          elseif playerType == 5 then
            hitTest = CheckCollision(v.x + v.width - 1, v.y + 1, 1, v.height - 2, x, y, 11, height)
          end
          if hitTest then
            if playerType == 1 then
              x = v.x + v.width - 9
            else
              x = v.x + v.width
            end
          end
        else
          if playerType == 1 then
            hitTest = CheckCollision(v.x + v.width - 1, v.y + 1, 1, v.height - 2, x - 27, y, width - 9, height)
          elseif playerType == 2 then
            hitTest = CheckCollision(v.x + v.width - 1, v.y + 1, 1, v.height - 2, x - 27, y, width, height)
          elseif playerType == 5 then
            hitTest = CheckCollision(v.x + v.width - 1, v.y + 1, 1, v.height - 2, x + 11, y, width, height)
          end
          if hitTest then
            if playerType == 1 then
              x = v.x + v.width + 36
            elseif playerType == 2 then
              x = v.x + v.width + 27
            elseif playerType == 5 then
              x = v.x + v.width - 11
            end
          end
        end
      end
    end
    v.posX = x
  end
end

function PlayerSprite() -- dan
  for i,v in ipairs(players) do
    if v.playerType == 1 then
      if v.state == 3 then
        if v.frames == 25 then
          if v.sprite == 3 then
            v.sprite = 0
            v.frames = 30
            v.state = 0
          else
            v.sprite = v.sprite + 1
          end
        elseif v.frames == 20 then
          if v.sprite == 3 then
            v.sprite = 0
            v.frames = 30
            v.state = 0
          else
            v.sprite = v.sprite + 1
          end
        elseif v.frames == 15 then
          if v.sprite == 3 then
            v.sprite = 0
            v.frames = 30
            v.state = 0
          else
            v.sprite = v.sprite + 1
          end
        elseif v.frames == 10 then
          if v.sprite == 3 then
            v.sprite = 0
            v.frames = 30
            v.state = 0
          else
            v.sprite = v.sprite + 1
          end
        elseif v.frames == 5 then
          if v.sprite == 3 then
            v.sprite = 0
            v.frames = 30
            v.state = 0
          else
            v.sprite = v.sprite + 1
          end
        elseif v.frames == 0 then
          if v.sprite == 3 then
            v.sprite = 0
            v.frames = 30
            v.state = 0
          else
            v.sprite = v.sprite + 1
          end
        end
        v.quad = love.graphics.newQuad(v.sprite * 28, 0, 28, 38, 336, 38)
      end
    elseif v.playerType == 2 then
      if v.state == 0 then
        if v.frames == 20 then
          if v.sprite == 7 then
            v.sprite = 0
          else
            v.sprite = v.sprite + 1
          end
        elseif v.frames == 10 then
          if v.sprite == 7 then
            v.sprite = 0
          else
            v.sprite = v.sprite + 1
          end
        elseif v.frames == 0 then
          if v.sprite == 7 then
            v.sprite = 0
          else
            v.sprite = v.sprite + 1
          end
        end
        if v.standing then
          v.quad = love.graphics.newQuad(252, 0, 28, 38, 308, 38)
        else
          v.quad = love.graphics.newQuad(v.sprite * 28, 0, 28, 38, 308, 38)
        end
      elseif v.state == 1 then
        v.sprite = 10
        v.quad = love.graphics.newQuad(v.sprite * 28, 0, 28, 38, 308, 38)
      elseif v.state == 2 then
        v.sprite = 10
        v.quad = love.graphics.newQuad(v.sprite * 28, 0, 28, 38, 308, 38)
      end
    elseif v.playerType == 5 then
      if v.state == 0 then --player is moving, switch between standing and crouching sprites
        if v.frames == 0 then
          if v.sprite == 0 then
            v.quad = love.graphics.newQuad(26, 0, 26, 38, 104, 38)
            v.sprite = 1
          elseif v.sprite == 1 then
            v.quad = love.graphics.newQuad(0, 0, 26, 38, 104, 38)
            v.sprite = 0
          end
        end
      elseif v.state == 1 then
        v.quad = love.graphics.newQuad(0, 0, 26, 38, 104, 38)
        v.sprite = 0
      elseif v.state == 2 then
        v.quad = love.graphics.newQuad(0, 0, 26, 38, 104, 38)
        v.sprite = 0
      elseif v.state == 3 then --player is attacking, go to attacking sprite and back via the transition sprite
        if v.sprite < 3 then
          v.quad = love.graphics.newQuad(52, 0, 26, 38, 104, 38)
          v.sprite = 2
        else
          v.executeRotate = true
          RotatePlayer()
          v.executeRotate = false
        end
        if v.frames == 0 then
          if v.toStanding then --for the transition back to moving
            v.quad = love.graphics.newQuad(26, 0, 26, 38, 104, 38)
            v.sprite = 1
            v.state = 0
            v.toStanding = false
          else
            v.quad = love.graphics.newQuad(78, 0, 26, 38, 104, 38)
            v.sprite = 3
            RotatePlayer()
          end
        end
      end
    end
  end
end

function PlayerBeltCheck() -- check if player is on a conveyer belt. dan
  for i,v in ipairs(players) do
    dir = v.dir
    x = v.posX
    y = v.posY
    width = v.width
    height = v.height
    playerType = v.playerType
    
    onBelt = false
    for i,v in ipairs(objects) do
      if v.objectType == 1 then
        if dir == -1 then
          if playerType == 1 then
            hitTest = CheckCollision(v.x + 1, v.y, v.width - 2, 1, x + 9, y, width - 9, height + 1)
          else
            hitTest = CheckCollision(v.x + 1, v.y, v.width - 2, 1, x, y, width, height + 1)
          end
        else
          if playerType == 1 then
            hitTest = CheckCollision(v.x + 1, v.y, v.width - 2, 1, x - 27, y, width - 9, height + 1)
          elseif playerType == 2 then
            hitTest = CheckCollision(v.x + 1, v.y, v.width - 2, 1, x - 27, y, width, height + 1)
          elseif playerType == 5 then
            hitTest = CheckCollision(v.x + 1, v.y, v.width - 2, 1, x + 11, y, width, height + 1)
          end
        end
        if hitTest then
          onBelt = true
        end
      end
    end
    if onBelt then
      x = x + 1
    end
    v.onBelt = onBelt
    v.posX = x
  end
end

function PlayerMove() -- dan
  for i,v in ipairs(players) do
    if v.playerType == 2 then
      v.standing = true
    end
  end
    
  if playerCanMove then
    if playerMoveDirection == -1 then
      for i,v in ipairs(players) do
        if player == v.playerType then
          if v.state ~= 3 then
            if v.dir == 1 then -- if player is turning from right to left, increase player X position by difference between origins
              if v.playerType == 1 then
                v.posX = v.posX - 36
              elseif v.playerType == 2 then
                v.posX = v.posX - 27
              elseif v.playerType == 5 then -- use for snowman move
                v.posX = v.posX + 11
              end
            end
            v.dir = -1
            if player == v.playerType then
              if v.posX < 120 then
                if screenMoved > 0 then
                  if screenMoved - v.speed < 0 then
                    distance = screenMoved
                  else
                      distance = v.speed
                  end
                  screenMoved = screenMoved - distance
                  for i,v in ipairs(players) do
                    v.posX = v.posX + distance
                  end
                  for i,v in ipairs(objects) do
                    v.x = v.x + distance
                  end
                  for i,v in ipairs(snowballs) do
                    v.x = v.x + distance
                  end
                end
              end
            end
            v.posX = v.posX + (v.speed * v.dir)
            if v.playerType == 2 then
              v.standing = false
            end
            if v.posX <= 0 - screenMoved then
              v.posX = 0 - screenMoved
            end
          end
        end
      end
    end
    
    if playerMoveDirection == 1 then
      for i,v in ipairs(players) do
        if player == v.playerType then
          if v.state ~= 3 then
            if v.dir == -1 then -- if player is turning from left to right, decrease player X position by difference between origins
              if v.playerType == 1 then
                v.posX = v.posX + 36
              elseif v.playerType == 2 then
                v.posX = v.posX + 27
              elseif v.playerType == 5 then
                v.posX = v.posX - 11
              end
            end
            v.dir = 1
            
            if player == v.playerType then
              if v.posX > 360 then
                if screenMoved + v.speed > 480 then
                  distance = 480 - screenMoved
                else
                  distance = v.speed
                end
                screenMoved = screenMoved + distance
                for i,v in ipairs(players) do
                  v.posX = v.posX - distance
                end
                for i,v in ipairs(objects) do
                  v.x = v.x - distance
                end
                for i,v in ipairs(snowballs) do
                  v.x = v.x - distance
                end
              end
            end
            
            v.posX = v.posX + (v.speed * v.dir)
            if v.playerType == 2 then
              v.standing = false
            end
            if v.playerType == 1 then
              if v.posX >= 489 then
                v.posX = 489
              end
            elseif v.playerType == 2 then
              if v.posX >= 480 then
                v.posX = 480
              end
            elseif v.playerType == 5 then
              if v.posX + 11 >= 480 - v.width then
                v.posX = 480 - v.width - 11
              end
            end
          end
        end
      end
    end
  end
end

function PlayerJump() -- dan
  for i,v in ipairs(players) do
    if v.state == 1 then
      if v.jumpHeight <= 0 then
        v.state = 2
      end
      v.posY = v.posY - v.jumpHeight
      v.jumpHeight = v.jumpHeight - 1
    end
  end
end

function PlayerFall() -- dan
  for i,v in ipairs(players) do
    if v.state == 2 then
      if v.posY >= v.previousGround - v.height then
        v.jumpHeight = 0
        v.state = 0
        v.posY = v.previousGround - v.height
        if v.playerType == 2 then
          v.sprite = 0
        end
      end
      v.posY = v.posY - v.jumpHeight
      v.jumpHeight = v.jumpHeight - 1
    end
  end
end

function PlayerAttack() -- dan
  for i,v in ipairs(players) do
    if v.state == 3 then
      if v.playerType == 1 then
        v.executeAttack = true
        SkeletonAttack()
        v.executeAttack = false
      elseif v.playerType == 5 then
        if v.sprite == 3 then
          if v.frames == 0 then
            v.executeAttack = true
            SnowmanAttack()
            v.executeAttack = false
          end
        end
      end
    end
    if v.playerType == 2 then
      if v.attacking then
        v.executeAttack = true
        WitchAttack()
        v.executeAttack = false
      end
      v.attacking = false
    end
  end
end

function SkeletonAttack()
  for i,v in ipairs(players) do
    if v.playerType == 1 then
      if v.executeAttack then
        if v.dir == -1 then
          x = v.posX
        else
          x = v.posX - 9
        end
        y = v.posY
        width = 9
        height = v.height
        
        for i,v in ipairs(players) do
          if v.playerType == 5 then
            if v.dir == -1 then
              hitTest = CheckCollision(x, y, width, height, v.posX, v.posY, v.width, v.height)
            else
              hitTest = CheckCollision(x, y, width, height, v.posX + 11, v.posY, v.width, v.height)
            end
            if hitTest then
              if v.invincibilityFrames == 0 then
                v.health = v.health - 20
                v.invincibilityFrames = 30
              end
            end
          end
        end
      end
    end
  end
end

function WitchAttack()
  for i,v in ipairs(players) do
    if v.playerType == 2 then
      if v.executeAttack then
        local fireball = {}
        fireball.dir = v.dir
        if fireball.dir == -1 then
          fireball.x = v.posX
          fireball.y = v.posY + 3
        else
          fireball.x = v.posX - 6
          fireball.y = v.posY + 3
        end
        if fireball.dir == -1 then
          fireball.angleY = (0 * 180 / math.pi * 10 / 9) / 20
        else
          fireball.angleY = ((1 / 2 * math.pi - ((math.pi * 2) - 3 / 2 * math.pi)) * 180 / math.pi * 10 / 9) / 20
        end
        fireball.angleX = 5 - fireball.angleY
        fireball.frames = 0
        fireball.interact = false -- do not involve in collisions until fireball is away from the player
        table.insert(fireballs, fireball)
        fireballSound:stop()
        fireballSound:play()
      end
    end
  end
end

function SnowmanAttack() -- added shooting. joe
  for i,v in ipairs(players) do
    if v.playerType == 5 then
      if v.executeAttack then
        if v.rotating then
          local shot = {}
          shot.x = v.posX + 15.5
          shot.y = v.posY + 25
          shot.dir = v.dir
          if shot.dir == -1 then
            shot.angleY = (v.rot * 180 / math.pi * 10 / 9) / 20
          else
            shot.angleY = ((1 / 2 * math.pi - (v.rot - 3 / 2 * math.pi)) * 180 / math.pi * 10 / 9) / 20
          end
          shot.angleX = 5 - shot.angleY
          shot.frames = 0
          shot.interact = false -- do not involve in collisions until snowball is away from the player. 0 for false
          shot.snowmanIdentity = v.identity
          table.insert(snowballs, shot)
          snowCannonSound:play()
        end
      end
    end
  end
end

function UpdateBelts()
  for i,v in ipairs(objects) do
    if v.objectType == 1 then
      v.timer = v.timer + 1
      if v.timer == 7 then 
        v.sprite = 1
        v.quad = love.graphics.newQuad(46, 0, 46, 10, 184, 10)
      elseif v.timer == 14 then
        v.sprite = 2
        v.quad = love.graphics.newQuad(92, 0, 46, 10, 184, 10)
      elseif v.timer == 21 then
        v.sprite = 3
        v.quad = love.graphics.newQuad(138, 0, 46, 10, 184, 10)
      elseif v.timer == 28 then
        v.sprite = 0
        v.timer = 0
        v.quad = love.graphics.newQuad(0, 0, 46, 10, 184, 10)
      end
    end
  end
end

function UpdateSnowballs() -- update snowballs. joe
  for i,v in ipairs(snowballs) do
    if v.dir == -1 then
      v.x = v.x - v.angleX
    else
      v.x = v.x + v.angleX
    end
    v.y = (v.y - v.angleY) + v.frames
    v.frames = v.frames + 0.05
    
    -- remove snowballs that are off screen
    if v.y > groundHeight then
      table.remove(snowballs, i)
    end
    
    -- remove snowballs that hit objects
    x = v.x
    y = v.y
    interact = v.interact
    hitObject = false
    snowmanIdentity = v.snowmanIdentity
    if interact == false then
      for i,v in ipairs(players) do
        if v.playerType == 5 then
          if snowmanIdentity == v.identity then
            if v.dir == -1 then
              hitTest = CheckCollision(x, y, 6, 6, v.posX, v.posY, v.width, v.height)
            else
              hitTest = CheckCollision(x, y, 6, 6, v.posX + 11, v.posY, v.width, v.height)
            end
            if hitTest == false then
              interact = true
            end
          end
        end
      end
      v.interact = interact
    end
    
    if v.interact then
      for i,v in ipairs(players) do
        if v.dir == -1 then
          if v.playerType == 1 then
            hitTest = CheckCollision(x, y, 6, 6, v.posX + 9, v.posY, v.width - 9, v.height)
          else
            hitTest = CheckCollision(x, y, 6, 6, v.posX, v.posY, v.width, v.height)
          end
        else
          if v.playerType == 1 then
            hitTest = CheckCollision(x, y, 6, 6, v.posX - 27, v.posY, v.width - 9, v.height)
          elseif v.playerType == 2 then
            hitTest = CheckCollision(x, y, 6, 6, v.posX - 27, v.posY, v.width, v.height)
          elseif v.playerType == 5 then
            hitTest = CheckCollision(x, y, 6, 6, v.posX + 11, v.posY, v.width, v.height)
          end
        end
        if hitTest then
          hitObject = true
          v.health = v.health - 5
          snowballHitSound:stop()
          snowballHitSound:play()
        end
      end
      
      for i,v in ipairs(objects) do
        hitTest = CheckCollision(x, y, 6, 6, v.x, v.y, v.width, v.height)
        if hitTest then
          hitObject = true
        end
      end
      if hitObject then
        table.remove(snowballs, i)
      end
    end
  end
end

function UpdateFireballs()
  for i,v in ipairs(fireballs) do
    if v.dir == -1 then
      v.x = v.x - v.angleX
    else
      v.x = v.x + v.angleX
    end
    v.y = (v.y - v.angleY) + v.frames
    v.frames = v.frames + 0.05
    
    -- remove fireballs that are off screen
    if v.y > groundHeight then
      table.remove(fireballs, i)
    end
    
    -- remove fireballs that hit objects
    x = v.x
    y = v.y
    interact = v.interact
    hitObject = false
    if interact == false then
      for i,v in ipairs(players) do
        if v.playerType == 2 then
          if v.dir == -1 then
            hitTest = CheckCollision(x, y, 6, 6, v.posX, v.posY, v.width, v.height)
          else
            hitTest = CheckCollision(x, y, 6, 6, v.posX - v.width, v.posY, v.width, v.height)
          end
          if hitTest == false then
            interact = true
          end
        end
      end
      v.interact = interact
    end
    
    if v.interact then
      for i,v in ipairs(players) do
        if v.dir == -1 then
          if v.playerType == 1 then
            hitTest = CheckCollision(x, y, 6, 6, v.posX + 9, v.posY, v.width - 9, v.height)
          else
            hitTest = CheckCollision(x, y, 6, 6, v.posX, v.posY, v.width, v.height)
          end
        else
          if v.playerType == 1 then
            hitTest = CheckCollision(x, y, 6, 6, v.posX - 27, v.posY, v.width - 9, v.height)
          elseif v.playerType == 2 then
            hitTest = CheckCollision(x, y, 6, 6, v.posX - 27, v.posY, v.width, v.height)
          elseif v.playerType == 5 then
            hitTest = CheckCollision(x, y, 6, 6, v.posX + 11, v.posY, v.width, v.height)
          end
        end
        if hitTest then
          hitObject = true
          v.health = v.health - 5
        end
      end
      
      for i,v in ipairs(objects) do
        hitTest = CheckCollision(x, y, 6, 6, v.x, v.y, v.width, v.height)
        if hitTest then
          hitObject = true
        end
      end
      if hitObject then
        table.remove(fireballs, i)
      end
    end
  end
end

function RotatePlayer()
  for i,v in ipairs(players) do
    if v.playerType == 5 then
      if v.executeRotate then
        --x, y = love.mouse.getPosition()
        x = 250
        y = 150
        if y <= v.posY then
          if v.dir == -1 then
            if x < v.posX then
              v.rot = CalculateRotate(v.posX, v.posY, x, y)
              v.rotating = true
            else
              v.rotating = false
            end
          else
            if x > v.posX then
              v.rot = CalculateRotate(v.posX, v.posY, x, y) + math.pi
              v.rotating = true
            else
              v.rotating = false
            end
          end
        else
          v.rotating = false
        end
      end
    end
  end
end

function CheckCollision(x1, y1, w1, h1, x2, y2, w2, h2)
  return x1 < x2 + w2 and
         x2 < x1 + w1 and
         y1 < y2 + h2 and
         y2 < y1 + h1
end

function CalculateRotate(x1, y1, x2, y2) --using trigonometry. dan
  xl = x1 - x2
  yl = y1 - y2
  r = math.atan2(yl, xl)
  return r
end